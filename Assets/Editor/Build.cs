﻿using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;

// ReSharper disable once CheckNamespace
// ReSharper disable once UnusedType.Global
public static class Build
{
    // ReSharper disable once UnusedMember.Global
    public static void WebGL()
    {
        PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Disabled;

        var report = BuildPipeline.BuildPlayer(
            EnabledScenes(),
            "dist/webgl",
            BuildTarget.WebGL,
            BuildOptions.None);

        if (report.summary.result != BuildResult.Succeeded) EditorApplication.Exit(1);
    }

    // ReSharper disable once UnusedMember.Global
    public static void Aab()
    {
        EditorUserBuildSettings.buildAppBundle = true;

        BuildPipeline.BuildPlayer(
            EnabledScenes(),
            "dist/app.aab",
            BuildTarget.Android,
            BuildOptions.None);
    }

    private static string[] EnabledScenes()
    {
        return EditorBuildSettings.scenes
            .Where(s => s.enabled)
            .Select(s => s.path)
            .ToArray();
    }
}